import 'package:flutter/material.dart';
import 'package:flutter_bloc/src/bloc_provider.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:kwaidi_casher/core/utils/app_colors.dart';
import 'package:kwaidi_casher/features/home/views/managers/main_cubit/main_cubit.dart';

class AppConfig {
  static List<BlocProviderSingleChildWidget> providers = [
    BlocProvider(create: (context) => MainCubit()),
  ];

  static Iterable<LocalizationsDelegate<dynamic>> settingsLocalizations =
      const [
    AppLocalizations.delegate, // Add this line
    GlobalMaterialLocalizations.delegate,
    GlobalWidgetsLocalizations.delegate,
    GlobalCupertinoLocalizations.delegate,
  ];

  static Iterable<Locale> supportedLocales = const [
    Locale('en'), // English, no country code
    Locale('ar'), // arabic, no country code
  ];

  static ThemeData? theme = ThemeData(
    primarySwatch: AppColors.APP_THEME,
  );
}
