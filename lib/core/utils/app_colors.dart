// ignore_for_file: constant_identifier_names, non_constant_identifier_names

import 'package:flutter/material.dart';

class AppColors {
  static const MaterialColor APP_THEME = MaterialColor(
    0xff3ac2cb,
    <int, Color>{
      50: Color(0xFFE0F8FA),
      100: Color(0xFFBAF6FA),
      200: Color(0xFF8EF0F6),
      300: Color(0xFF62E8F1),
      400: Color(0xFF40E4EF),
      500: Color(0xff38bdc5),
      600: Color(0xFF1BD2DE),
      700: Color(0xFF17C1CC),
      800: Color(0xFF14B1BB),
      900: Color(0xFF0C919A),
    },
  );

  static const Color TRANSPARENT = Color.fromARGB(0, 255, 255, 255);
  static const Color BG_SCAFFOLD_DARK_MODE = Color(0xFF040d21);

  static const Color WHITE = Color(0xFFFFFFFF);
  static const Color WHITE2 = Color(0xFFFAFAFA);

  static const Color BLACK = Color(0xFF000000);
  static const Color BG_DRAWER = Color(0xFF16263F);
  static const Color BG_ITEM_DRAWER = Color(0xFF36415C);

  static const Color PURPLE = Color(0xff5059CC);

  static const Color BASE_COLOR = Color(0xff3ac2cb);
  static const Color GRAY = Color(0xFFA4B4CB);
  static const Color BORDER = Color(0xff546076);

  static const Color RED = Color(0xFFCD1717);
  static const Color ORANGE = Color(0xFFFE9518);
  static const Color GREEN = Color(0xFF34B53A);
}
