import 'package:bitsdojo_window/bitsdojo_window.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:kwaidi_casher/core/responsive/responsive.dart';
import 'package:kwaidi_casher/core/utils/app_colors.dart';
import 'package:kwaidi_casher/core/widgets/my_contianer_shape.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kwaidi_casher/core/widgets/my_text.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:kwaidi_casher/features/home/views/managers/main_cubit/main_cubit.dart';

class MyDrawer extends StatelessWidget {
  const MyDrawer({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 240,
      height: double.infinity,
      child: Container(
        color: AppColors.BG_DRAWER,
        child: BlocBuilder<MainCubit, MainState>(
          builder: (context, state) {
            return SingleChildScrollView(
              child: Column(
                children: [
                  if (Responsive.isMobile(context))
                    WindowTitleBarBox(
                      child: MoveWindow(),
                    ),
                  const SizedBox(height: 26),
                  Image.asset(
                    'assets/images/logo.png',
                    width: 201,
                    fit: BoxFit.cover,
                  ),
                  const SizedBox(height: 55),
                  ItemDrawer(
                    selected: MainCubit.get(context).selectedItemDrawer == 0,
                    iconPath: 'assets/images/icon_drawer/ic_dashboard.svg',
                    title: AppLocalizations.of(context)!.dashboard,
                    onTap: () {
                      MainCubit.get(context).changeSelectedItemDrawer(0);
                    },
                  ),
                  const SizedBox(height: 40),
                  ItemDrawer(
                    selected: MainCubit.get(context).selectedItemDrawer == 1,
                    iconPath: 'assets/images/icon_drawer/new_order.svg',
                    title: AppLocalizations.of(context)!.newOrder,
                    onTap: () {
                      MainCubit.get(context).changeSelectedItemDrawer(1);
                    },
                  ),
                  const SizedBox(height: 40),
                  ItemDrawer(
                    selected: MainCubit.get(context).selectedItemDrawer == 2,
                    iconPath: 'assets/images/icon_drawer/online_order.svg',
                    title: AppLocalizations.of(context)!.onlineOrder,
                    onTap: () {
                      MainCubit.get(context).changeSelectedItemDrawer(2);
                    },
                  ),
                  const SizedBox(height: 40),
                  ItemDrawer(
                    selected: MainCubit.get(context).selectedItemDrawer == 3,
                    iconPath: 'assets/images/icon_drawer/invoice.svg',
                    title: AppLocalizations.of(context)!.invoices,
                    onTap: () {
                      MainCubit.get(context).changeSelectedItemDrawer(3);
                    },
                  ),
                  const SizedBox(height: 40),
                  ItemDrawer(
                    selected: MainCubit.get(context).selectedItemDrawer == 4,
                    iconPath: 'assets/images/icon_drawer/explanations.svg',
                    title: AppLocalizations.of(context)!.explanations,
                    onTap: () {
                      MainCubit.get(context).changeSelectedItemDrawer(4);
                    },
                  ),
                  const SizedBox(height: 40),
                  ItemDrawer(
                    selected: MainCubit.get(context).selectedItemDrawer == 5,
                    iconPath: 'assets/images/icon_drawer/category.svg',
                    title: AppLocalizations.of(context)!.categories,
                    onTap: () {
                      MainCubit.get(context).changeSelectedItemDrawer(5);
                    },
                  ),
                  const SizedBox(height: 40),
                  ItemDrawer(
                    selected: MainCubit.get(context).selectedItemDrawer == 6,
                    iconPath: 'assets/images/icon_drawer/logout.svg',
                    title: AppLocalizations.of(context)!.logOut,
                    onTap: () {
                      MainCubit.get(context).changeSelectedItemDrawer(6);
                    },
                  ),
                  const SizedBox(height: 40),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}

class ItemDrawer extends StatelessWidget {
  final String iconPath;
  final String title;
  final GestureTapCallback? onTap;
  final bool selected;

  const ItemDrawer({
    Key? key,
    required this.iconPath,
    required this.title,
    required this.onTap,
    required this.selected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: selected ? AppColors.BASE_COLOR : AppColors.BG_ITEM_DRAWER,
      borderRadius: BorderRadius.circular(30),
      child: InkWell(
        borderRadius: BorderRadius.circular(30),
        onTap: onTap,
        child: MyContainerShape(
          height: 144,
          width: 144,
          enableShadow: false,
          enableBorder: false,
          bgContainer: AppColors.TRANSPARENT,
          borderRadius: 30,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SvgPicture.asset(
                iconPath,
              ),
              const SizedBox(height: 22),
              MyText(title: title)
            ],
          ),
        ),
      ),
    );
  }
}
