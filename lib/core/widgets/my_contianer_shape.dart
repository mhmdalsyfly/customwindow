import 'package:flutter/material.dart';
import 'package:kwaidi_casher/core/utils/app_colors.dart';

class MyContainerShape extends StatelessWidget {
  Widget? child;
  Color shadow;
  Color? bgContainer;
  Color colorBorder;
  bool enableShadow;
  bool enableRadius;
  bool enableBorder;
  BoxShape boxShape;
  AlignmentDirectional? alignment;
  double? height;
  double? width;
  double widthBorder;
  double paddingVertical;
  double paddingHorizontal;
  double borderRadius;
  double xOffset;
  double yOffset;
  double blur;
  double marginTop;
  double marginBottom;
  double marginStart;
  double marginEnd;

  MyContainerShape({
    this.height,
    this.width,
    this.boxShape = BoxShape.rectangle,
    this.paddingVertical = 0,
    this.paddingHorizontal = 0,
    this.marginTop = 0,
    this.marginBottom = 0,
    this.marginStart = 0,
    this.marginEnd = 0,
    this.borderRadius = 30,
    this.xOffset = 0,
    this.widthBorder = 1,
    this.yOffset = 10,
    this.blur = 18,
    this.alignment = AlignmentDirectional.center,
    this.enableShadow = true,
    this.enableRadius = true,
    this.enableBorder = false,
    this.shadow = AppColors.WHITE,
    this.bgContainer = AppColors.BASE_COLOR,
    this.colorBorder = AppColors.BORDER,
    this.child,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      clipBehavior: Clip.antiAlias,
      alignment: alignment,
      padding: EdgeInsets.symmetric(
        vertical: paddingVertical,
        horizontal: paddingHorizontal,
      ),
      margin: EdgeInsetsDirectional.only(
        top: marginTop,
        bottom: marginBottom,
        start: marginStart,
        end: marginEnd,
      ),
      decoration: BoxDecoration(
        color: bgContainer,
        shape: boxShape,
        borderRadius: enableRadius ? BorderRadius.circular(borderRadius) : null,
        boxShadow: enableShadow
            ? [
                BoxShadow(
                  color: shadow,
                  offset: Offset(xOffset, yOffset),
                  blurRadius: blur,
                ),
              ]
            : null,
        border: enableBorder
            ? Border.all(color: colorBorder, width: widthBorder)
            : null,
      ),
      child: child,
    );
  }
}
