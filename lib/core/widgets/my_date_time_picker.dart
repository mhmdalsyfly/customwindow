import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:kwaidi_casher/core/utils/app_colors.dart';
import 'package:kwaidi_casher/core/widgets/my_contianer_shape.dart';

class MyDateTimePicker extends StatelessWidget {
  const MyDateTimePicker({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MyContainerShape(
      width: 176,
      height: 60,
      alignment: AlignmentDirectional.center,
      enableRadius: true,
      enableBorder: true,
      enableShadow: false,
      bgContainer: AppColors.TRANSPARENT,
      borderRadius: 10,
      widthBorder: 1,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            width: 120,
            child: DateTimePicker(
              textAlign: TextAlign.center,
              cursorColor: AppColors.BASE_COLOR,
              initialValue: DateTime.now().toString(),
              style: const TextStyle(color: AppColors.GRAY),
              firstDate: DateTime(2000),
              lastDate: DateTime(2100),
              decoration: const InputDecoration(
                enabledBorder: InputBorder.none,
                focusedBorder: InputBorder.none,
                focusedErrorBorder: InputBorder.none,
                errorBorder: InputBorder.none,
              ),
              dateLabelText: '',
              fieldHintText: '',
              onChanged: (val) {
                print(val);
              },
              validator: (val) {
                print(val);
                return null;
              },
              onSaved: (val) {
                print(val);
              },
            ),
          ),
          SvgPicture.asset('assets/images/arrows/arrow-down.svg')
        ],
      ),
    );
  }
}
