import 'package:bitsdojo_window/bitsdojo_window.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kwaidi_casher/app_config.dart';
import 'package:kwaidi_casher/features/home/views/pages/main_screen.dart';

void main() {
  runApp(const MyApp());
  // ! setup custom window in app
  doWhenWindowReady(() {
    const initialSize = Size(1400, 450);
    // appWindow.minSize = initialSize;
    appWindow.size = initialSize;
    appWindow.alignment = Alignment.center;
    appWindow.title = 'Custom';
    appWindow.show();
  });
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(1920, 1080),
      minTextAdapt: true,
      splitScreenMode: true,
      builder: (context, child) {
        return MultiBlocProvider(
          providers: AppConfig.providers,
          child: MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'Kwaidi Casher',
            localizationsDelegates: AppConfig.settingsLocalizations,
            supportedLocales: AppConfig.supportedLocales,
            locale: const Locale('en'),
            theme: AppConfig.theme,
            home: child,
          ),
        );
      },
      child: const MainScreen(),
    );
  }
}
