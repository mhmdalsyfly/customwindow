import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

part 'main_state.dart';

class MainCubit extends Cubit<MainState> {
  GlobalKey<ScaffoldState> drawerKey = GlobalKey();

  int selectedItemDrawer = 0;

  MainCubit() : super(MainInitial());

  static MainCubit get(BuildContext context) {
    return BlocProvider.of(context);
  }

  changeSelectedItemDrawer(int index) {
    selectedItemDrawer = index;
    emit(MainInitial());
  }
}
