import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:kwaidi_casher/core/responsive/responsive.dart';
import 'package:kwaidi_casher/core/utils/app_colors.dart';
import 'package:kwaidi_casher/core/widgets/my_contianer_shape.dart';
import 'package:kwaidi_casher/core/widgets/my_date_time_picker.dart';
import 'package:kwaidi_casher/core/widgets/my_text.dart';
import 'package:kwaidi_casher/features/home/views/widgets/dashboard/cashier_statistics_box.dart';
import 'package:kwaidi_casher/features/home/views/widgets/main_custom_appbar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class MainBody extends StatelessWidget {
  const MainBody({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (Responsive.isDesktop(context)) const SizedBox(height: 30),
              const MainCustomAppBar(),
              Padding(
                padding: EdgeInsetsDirectional.only(start: 50.w, end: 30.w),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    MyText(
                      title: '26.octobar.2022  Wednesday | 10:40 pm',
                      fontSize: 24,
                      fontWeight: FontWeight.w400,
                    ),
                    SizedBox(height: 32.h),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        MyText(
                          title:
                              AppLocalizations.of(context)!.cashierStatistics,
                          fontSize: 24,
                          fontWeight: FontWeight.w500,
                        ),
                        Row(
                          children: [
                            const MyDateTimePicker(),
                            SizedBox(width: 30.w),
                            MyText(
                              title: AppLocalizations.of(context)!.from,
                              fontSize: 20,
                              fontWeight: FontWeight.w500,
                              color: AppColors.GRAY,
                            ),
                            SizedBox(width: 20.w),
                            const MyDateTimePicker(),
                            SizedBox(width: 30.w),
                            MyText(
                              title: AppLocalizations.of(context)!.to,
                              fontSize: 20,
                              fontWeight: FontWeight.w500,
                              color: AppColors.GRAY,
                            ),
                            SizedBox(width: 20.w),
                            const MyDateTimePicker(),
                          ],
                        )
                      ],
                    ),
                    SizedBox(height: 50.h),
                    Row(
                      children: [
                        Expanded(
                          child: CashierStatisticsBox(
                            titleKey:
                                AppLocalizations.of(context)!.cashierStatistics,
                            color: AppColors.BASE_COLOR,
                            value: '100.00 SR',
                          ),
                        ),
                        SizedBox(width: 125.w),
                        Expanded(
                          child: CashierStatisticsBox(
                            titleKey: AppLocalizations.of(context)!.totalOrders,
                            color: AppColors.ORANGE,
                            value: '7000 order',
                          ),
                        ),
                        SizedBox(width: 125.w),
                        Expanded(
                          child: CashierStatisticsBox(
                            titleKey: AppLocalizations.of(context)!.activeOrder,
                            color: AppColors.GREEN,
                            value: '270 order',
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 50.h),
                    Row(
                      children: [
                        MyContainerShape(
                          height: 200,
                          width: 980.w,
                          xOffset: 0,
                          yOffset: 0,
                          shadow: AppColors.BASE_COLOR.withOpacity(.4),
                          enableRadius: true,
                          blur: 15,
                          borderRadius: 10,
                          bgContainer: AppColors.BG_DRAWER,
                        ),
                        SizedBox(width: 125.w),
                        Expanded(
                          child: MyContainerShape(
                            height: 200,
                            xOffset: 0,
                            yOffset: 0,
                            shadow: AppColors.BASE_COLOR.withOpacity(.4),
                            enableRadius: true,
                            blur: 15,
                            borderRadius: 10,
                            bgContainer: AppColors.BG_DRAWER,
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
