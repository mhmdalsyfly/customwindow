import 'package:flutter/material.dart';
import 'package:kwaidi_casher/core/responsive/responsive.dart';
import 'package:kwaidi_casher/core/utils/app_colors.dart';
import 'package:kwaidi_casher/core/widgets/my_text.dart';
import 'package:kwaidi_casher/features/home/views/managers/main_cubit/main_cubit.dart';

class MainCustomAppBar extends StatelessWidget {
  const MainCustomAppBar({super.key});

  @override
  Widget build(BuildContext context) {
    return Responsive.isDesktop(context)
        ? PreferredSize(
            preferredSize: Size(double.infinity, AppBar().preferredSize.height),
            child: const SizedBox(),
          )
        : AppBar(
            backgroundColor: AppColors.WHITE.withOpacity(.3),
            centerTitle: true,
            elevation: 0,
            title: MyText(
              title: 'SmartSpace',
              fontWeight: FontWeight.bold,
              fontSize: 22,
            ),
            leading: IconButton(
              icon: const Icon(
                Icons.menu,
                color: AppColors.WHITE,
              ),
              onPressed: () {
                MainCubit.get(context).drawerKey.currentState!.openDrawer();
              },
            ),
          );
  }
}
