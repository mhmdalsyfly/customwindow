import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:kwaidi_casher/core/utils/app_colors.dart';
import 'package:kwaidi_casher/core/widgets/my_contianer_shape.dart';
import 'package:kwaidi_casher/core/widgets/my_text.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CashierStatisticsBox extends StatefulWidget {
  final String titleKey;
  final String value;
  final Color color;

  CashierStatisticsBox({
    Key? key,
    required this.color,
    required this.titleKey,
    required this.value,
  }) : super(key: key);

  @override
  State<CashierStatisticsBox> createState() => _CashierStatisticsBoxState();
}

class _CashierStatisticsBoxState extends State<CashierStatisticsBox> {
  bool isHover = false;

  Offset mousPos = Offset(0, 0);

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      cursor: SystemMouseCursors.click,
      onEnter: (PointerEnterEvent e) {
        setState(() {
          isHover = true;
        });
      },
      onHover: (PointerHoverEvent event) {
        setState(() {
          mousPos += event.delta;
          mousPos *= .5;
        });
      },
      onExit: (PointerExitEvent e) {
        setState(() {
          isHover = false;
        });
      },
      child: AnimatedContainer(
        duration: const Duration(milliseconds: 500),
        height: isHover ? 160.h : 168.h,
        child: MyContainerShape(
          xOffset: 0,
          yOffset: 0,
          shadow: isHover
              ? AppColors.BASE_COLOR.withOpacity(.8)
              : AppColors.BASE_COLOR.withOpacity(.4),
          enableRadius: true,
          blur: 15,
          borderRadius: 10,
          bgContainer: AppColors.BG_DRAWER,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MyText(
                    title: widget.titleKey,
                    fontSize: 20.sp,
                    color: AppColors.GRAY,
                  ),
                  SizedBox(height: 30.h),
                  MyText(
                    title: widget.value,
                    fontSize: 32.sp,
                    color: widget.color,
                  ),
                ],
              ),
              SizedBox(width: 50.w),
              SvgPicture.asset(
                'assets/images/chart.svg',
                width: 175.w,
                color: widget.color,
              )
            ],
          ),
        ),
      ),
    );
  }
}
