import 'package:bitsdojo_window/bitsdojo_window.dart';
import 'package:flutter/material.dart';
import 'package:kwaidi_casher/core/responsive/responsive.dart';
import 'package:kwaidi_casher/core/responsive/size_config.dart';
import 'package:kwaidi_casher/core/utils/app_colors.dart';
import 'package:kwaidi_casher/core/widgets/my_drawer.dart';
import 'package:kwaidi_casher/core/widgets/my_text.dart';
import 'package:kwaidi_casher/core/widgets/window_buttons.dart';
import 'package:kwaidi_casher/features/home/views/managers/main_cubit/main_cubit.dart';
import 'package:kwaidi_casher/features/home/views/widgets/main_body.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: AppColors.BG_SCAFFOLD_DARK_MODE,
      key: MainCubit.get(context).drawerKey,
      drawer: const MyDrawer(),
      extendBodyBehindAppBar: true,
      body: Column(
        children: [
          WindowTitleBarBox(
            child: Row(
              children: [
                Expanded(child: MoveWindow()),
                const WindowButtons(),
              ],
            ),
          ),
          Expanded(
            child: Row(
              children: [
                if (Responsive.isDesktop(context)) const MyDrawer(),
                const MainBody(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
